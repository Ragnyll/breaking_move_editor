#!/bin/python

"""main.py
From a configuration file creates a video splicing together clips and creating subclips as necessary
"""
import fire
import json
import os
from pathlib import Path
from moviepy.editor import *


def convert_time_to_s(time):
    """Converts a time of format m*:ss to just seconds

    :param time: a time of for m*:ss
    :return: the time in seconds
    """
    times = [int(num) for num in time.split(':')]
    return times[0] * 60 + times[1]


def build_move(move, output_path='./moves'):
    """Builds a move from the configuration and outputs it at the output path.

    :param move: a move with format
        ```
        "moves": [
          {
            "move_name": "[move_name]",
            "from_clip": "(from_clip)",
            "start_time": "start_min:start_sec",
            "end_time": "end_min:end_sec",
            "keep_audio": true
          }
          ```
    :param output_path: the path at which a move file will be stored
    :return:
    """
    if not os.path.isdir(output_path):
        Path(output_path).mkdir(parents=True, exist_ok=True)

    keep_audio = 'keep_audio' in move.keys() and move['keep_audio'] is True

    if 'start_time' in move.keys() and 'end_time' in move.keys():
        start_time = convert_time_to_s(move['start_time'])
        end_time = convert_time_to_s(move['end_time'])
        move_clip = VideoFileClip(move['from_clip'], audio=keep_audio).subclip(start_time, end_time)
    else:
        move_clip = VideoFileClip(move['from_clip'], audio=keep_audio)

    if 'move_name' in move.keys():
        move_name_txt = TextClip(move['move_name'], font='Fira Code', color='white', fontsize=14)

        if 'start_time' in move.keys() and 'end_time' in move.keys():
            start_time = convert_time_to_s(move['start_time'])
            end_time = convert_time_to_s(move['end_time'])
            txt_col = move_name_txt.on_color(size=(move_clip.w + move_name_txt.w, move_name_txt.h + 10),
                                             color=(0, 0, 0), pos=(6, 'center'), col_opacity=0).subclip(0,
                                                                                                        end_time - start_time)
        else:
            txt_col = move_name_txt.on_color(size=(move_clip.w + move_name_txt.w, move_name_txt.h + 10),
                                             color=(0, 0, 0), pos=(6, 'center'), col_opacity=0).subclip(0,
                                                                                                        move_clip.duration)

        move_clip = CompositeVideoClip([move_clip, txt_col])
        # then try to make the file in the specified output path
        move_clip.write_videofile(os.path.join('./moves', move['move_name'] + '.mp4'))

    return move_clip


def build_combo(combo, combo_output_path='./combos', move_output_path='./moves'):
    """From the combo builds the respective combo and and moves and puts them in files

    :param combo: the configuration for a combo with format
        ```
        {
          "combo_name": "(combo)",
          "audio": "[audio_fname]",
          "moves": []
        }
        ```
    :param combo_output_path: the location at which to output combo footage
    :param move_output_path:  the location at which to output move footage
    :return: the concatenated video file of all the moves in the combo
    """
    move_clips = []
    for move in combo['moves']:
        move_clips.append(build_move(move, move_output_path))

    if not os.path.isdir(combo_output_path):
        Path(combo_output_path).mkdir(parents=True, exist_ok=True)

    concatenate_videoclips(move_clips).write_videofile(os.path.join(combo_output_path, combo['combo_name'] + '.mp4'))


def build_clips(combo_config_path, combo_output_path='./combos', move_output_path='./moves'):
    """From the combo_config_path file builds the combos and moves files outputing them to the respective dirs

    :param combo_config_path: The path to the json file containing the configuration for moves and combos. The json file is of format
        ```
        [
          {
            "combo_name": "(combo)",
            "audio": "[audio_fname]",
            "moves": [
              {
                "move_name": "[move_name]",
                "from_clip": "(from_clip)",
                "start_time": "start_min:start_sec",
                "end_time": "end_min:end_sec",
                "keep_audio": true
              }
            ]
          }
        ]
        ```
    :param combo_output_path: The path at which to store combo files
    :param move_output_path: The path at which to store moves
    :return:
    """
    with open(combo_config_path, 'rb') as combo_config_file:
        combo_config = json.load(combo_config_file)

    for combo in combo_config:
        build_combo(combo, combo_output_path, move_output_path)


def build_notebook(combo_config_path, notebook_output_path='./combos', notebook_name='notebook.md'):
    """Creates an output markdown file detailing combos and moves

    :param combo_config_path: the path where the combo_config file exists
    :param notebook_output_path: the path where to output the notebook
    :param notebook_name: the name of the notebook
    """
    with open(combo_config_path, 'rb') as combo_config_file:
        combo_config = json.load(combo_config_file)

    if not os.path.isdir(notebook_output_path):
        Path(notebook_output_path).mkdir(parents=True, exist_ok=True)

    notebook_file = open(os.path.join(notebook_output_path, notebook_name), 'w+')
    for combo in combo_config:
        notebook_file.write('# ' + combo['combo_name'] + '\n')
        for move in combo['moves']:
            notebook_file.write('* ' + move['move_name'] + '\n')
        notebook_file.write('\n')
    notebook_file.close()


def filter_for_qualifying_moves(combo_config_path, filter_tags, contains_all_tags=True, combo_output_path=None):
    """finds all moves that contains the given filter term. If contains_all_terms is False then all moves containing any of the given filter terms will be returned

    :param combo_config_path:
    :param filter_terms:
    :param contains_all_terms: combo must contain all the given tags to be qualified
    :param combo_output_path: the place to output a combo containing all the filtered moves

    :return: (list) all moves containing given tags
    """
    with open(combo_config_path, 'rb') as combo_config_file:
        combo_config = json.load(combo_config_file)

    filtered_moves = []
    for combo in combo_config:
        for moves in combo:
            filtered_moves.append([move for move in moves if 'tags' in move.keys() and filter_tags in move['tags']])

    return filtered_moves


def main():
    """Primary entry point for the script to manage footage. Call to fire manages CLI work

    :return:
    """
    fire.Fire({
        'build_clips': build_clips,
        'build_notebook': build_notebook,
        'find_moves': filter_for_qualifying_moves
    })


if __name__ == '__main__':
    main()
