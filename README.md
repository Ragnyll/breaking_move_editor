# _NOTE: THESE DOCS ARE OUT OF DATE! I NEED TO UPDATE THEM _#
If you wanna see the run instructions you can look at the template file, or run
```
python move_vid_builder.py -- --help
```

# Brkn Combo Builder
&nbsp;&nbsp;&nbsp;&nbsp;The Brkn Combo Builder is a small utility to build combo and move footage using json.
 This will create files for both the the combo (sequence of moves) and each of the component moves.

&nbsp;&nbsp;&nbsp;&nbsp;This is primarily meant for quick experimenting with different move sequences and orderings.
 Where standard video editing software requires usage of many window applications this relies only the CLI and config files.
 The end result is a JSON file that can serve as a quick means of editing these orders and component footages.

&nbsp;&nbsp;&nbsp;&nbsp;This is a command line utility solely for editing combo and move footage, not viewing or additional file manipulation.
 For a standard suite I suggest using a terminal where you know the directory path to the footage, ranger for browsing the generated and existing footage, and VLC for viewing the footage.

## Usage
```python3
# install dependencies
pip -r requirements.txt

# run
$PATH/source/main.py combo_config.json
```

## Configuration
```
# combo_config.json
# note that this config can be found in template.json
[
    {
        "combo_name": "combo",
        "moves": [
            {
                "move_name": "[move_name]",
                "from_clip": "from_clip",
                "start_time": "start_min:start_sec",
                "end_time": "end_min:end_sec"
				"keep_audio": true
            }
        ]
    }
]
```
where the number of combos and moves is arbitrary.

 If `move_name` is omitted then the component move will not have it's file created.

Valid Example:
This will create a combo file, but no moves files.
```
[
    {
        "combo_name": "combo",
        "moves": [
            {
                "from_clip": "from_clip",
                "start_time": "start_min:start_sec",
                "end_time": "end_min:end_sec"
            }
        ]
    }
]
```

Invalid Example:
Error will be thrown fron invalid combo name and invalid move name.
```
# combo_config.json
# note that this config can be found in template.json
[
    {
        "combo_name": "",
        "moves": [
            {
                "move_name": "",
                "from_clip": "from_clip",
                "start_time": "start_min:start_sec",
                "end_time": "end_min:end_sec"
            }
        ]
    }
]
```
